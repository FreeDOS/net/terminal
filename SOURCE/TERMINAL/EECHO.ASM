	org 0x100

start:
	mov si,0x81	; arg start
	xor cx,cx
	add cl,[si-1]	; arg length (not including final CR)
	jz helpmsg
showloop:
	lodsb
	cmp al,'$'	; replace this
	jnz easy
	mov al,27	; ESC
	nop		; could add "replace $$ by $ rather than ESCESC"
	nop
	nop
easy:
	call tty
	loop showloop
	mov al,13	; CR
	call tty
	mov al,10	; LF
	call tty
	int 0x20	; end prog

tty:
	mov ah,2
	mov dl,al
	int 0x21	; classic DOS tty
	ret

helpmsg:
	mov si,msg	; *offset* help message
	mov cx,msgend+1-msg	; length
	jmp short showloop

msg	db "Usage: eecho string (DOLLAR becomes ESC). By Eric Auer 10/2001"
msgend	db 13,10,0,"$     "	; never printed...
