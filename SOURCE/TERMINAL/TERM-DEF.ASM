; defines for use in terminal: macros and constants


; ---------------------------------------------------------------------

; * masm style constants: FOO = bar
; * nasm style constants: %[i]define FOO bar <- i means case insensitive

%define FLUCHT 3
; <- the shift chord to exit the terminal (3 is "lshift and rshift")

; ---------------------------------------------------------------------

%define RTSSEND 3
; 2 RTS 3 RTS+DTR <- how to send RTS

; ---------------------------------------------------------------------

; describing how to determine the time elapsed and when to time out:
; TIMEOUT = number of clocks, if WAITGFX defined use V/H sync, else timer
; as source. WAITBIT defines which byte of the used port is our "clock"
%define TIMEOUT 60
%define WAITBIT 8
; 8 is Vsync 1 is Hsync (if WAITGFX), 10h is timer (if not WAITGFX)
%define WAITGFX 1
; using graphics (sync) if defined - use port 61h (timer) else

; ---------------------------------------------------------------------

; * masm style macro: label macro [params] ... endm
; * nasm style macro: %[i]macro label num-of-params ... %endmacro
; *   params are used as %1 %2 ... inside the macro
; *   syntax also allows defaults and num-of-param as a range

; used in some I/O code:
%imacro	IODLY 0
	nop	; used as delay between I/O port accesses
%endmacro

; ---------------------------------------------------------------------

; used in TTY and ANSI:
; BX2DI: find DI (offset into screen memory) from (X,Y) coords (BL,BH)
%imacro BX2DI 0
        xchg di,ax	; save AX
        mov ax,[cs:cols]	; screen width
        mul bh		; use row
        add al,bl	; add column
        adc ah,0
        shl ax,1
        xchg di,ax	; restore AX: no registers are destroyed!
%endmacro

; nasm: %[i]define %include %ifdef/%ifndef/%else/%endif %elif[n]{def,ctx}
