; functions used to set up and use interrupt driven UART handling,
; including the interrupt handler itself!

IRQ_INST:
	push ax
        mov ax,IRQ_buf		; *offset*
        mov [IRQ_buf_a],ax	; the write pointer
        mov [IRQ_buf_b],ax	; the read pointer
        mov ax,[cs:comnum]	; which port are we using? (0..3)
		push di
        mov di,(4+8)*4		; IRQ 4 is interrupt 0ch
        mov ah,0ffh-10h		; bit mask for 8259, IRQ 4 enable
        test al,1       ; IRQ 4 for COM1/COM3 (0,2), else IRQ 3
        jz irq_is4
irq_is3: sub di,4	; we have IRQ 3
        ror ah,1	; change the bitmask: IRQ 3 enable
irq_is4:
	mov [IRQ_intpos],di	; store which interrupt we are hooking
	push ax		; contains our 8259 mask modifier
		push es
        xor ax,ax
        mov es,ax
        mov ax,[es:di]
        mov [IRQ_oldvect+0],ax
        mov ax,[es:di+2]
        mov [IRQ_oldvect+2],ax
        mov ax,cs
        cli
        mov word [es:di],IRQ_READIRQ	;  *offset*
        mov [es:di+2],ax	; now our handler is in place
        sti
		pop es
	pop ax		; contains our 8259 mask modifier
		pop di
        in al,21h
	mov [cs:IRQ_oldmask],al	; store old 8259 interrupt mask
        and al,ah       ; setting the right bit to 0 enables the IRQ
        out 21h,al      ; so tell the 8259 about it
		push dx
        mov dx,[cs:comdta]   ; Port
        add dx,7	; this is the scratch reg, if 16450 or newer UART
        mov ax,5555h
        out dx,al       ; write to scratch reg
		IODLY
        in al,dx	; is the data still there?
        cmp al,ah
        jnz irq_uralt   ; if not, we have an old 8250 UART
        mov dx,[cs:comdta]   ; Port
        add dx,2	; if yes, we activate the FIFO feature :-)
			; (write only port, reading gives int status)
        mov al,087h     ; *** 1=Enable (2/4=Rx/Tx Flush) 00,40,80,c0=Size FIFO
        out dx,al	; tell the UART
irq_uralt:
        mov dx,[cs:comdta]   ; Port
	inc dx		; the UART event mask (when to trigger an IRQ)
	in al,dx	; read current UART event mask
		IODLY
	mov [IRQ_oldevents],al	; store old UART event mask
	mov al,0dh	; <<< *** 1=if data arrived (2=if data can be sent)
		; <<< 4=if line status changed 8=if modem status changed
	out dx,al	; write new UART event mask
	mov dx,[cs:comdta]
	add dx,4	; modem control
	mov al,00001011b	; *** 00, ?, loopback(0 is off),
				;     OUT2 (serves as IRQ enable!!!)
				;     OUT1 (alternative clock, is off),
				;     RTS, DTR
	mov ah,0ffh
	mov [modlines],ax
	out dx,al
		pop dx
	pop ax
        RET


IRQ_UNINST:		; restores the old IRQ handler and the
	push ax		; IRQ related setup of UART and 8259
		push dx	; to be called to shut down interrupt driven I/O
	mov al,[cs:IRQ_oldevents]
	mov dx,[cs:comdta]
	inc dx
	out dx,al	; restore old UART interrupt event mask
		pop dx
	mov al,[cs:IRQ_oldmask]
	out 21h,al	; restore old 8259 interrupt mask
		push es
		push si
		push di
        xor ax,ax	; now restoring the old handler
        mov es,ax
        mov di,[cs:IRQ_intpos]
        mov si,IRQ_oldvect	; *offset*
        pushf
        cli
        movsw
        movsw
        popf
		pop di
		pop si
		pop es
	pop ax
        RET

; ---------------------------------------------------------------------

IRQ_nextpos:    ; helper function: increment BX in a circluar way
        inc bx
        cmp bx,IRQ_eofbuf	;  *offset*
        jnz irq_nextok
        mov bx,IRQ_buf		;  *offset*
irq_nextok:
        RET


IRQ_READBUF:			; read data from input buffer to AX,
	push bx			; return -1 if no data in buffer
	pushf
	cli			; better allow no interrupts here!
        mov bx,[cs:IRQ_buf_b]	; the read pointer
        cmp bx,[cs:IRQ_buf_a]	; the write pointer
	jz irq_empty1
        mov al,[bx]
        call IRQ_nextpos        ; BX circular + 1
        mov [IRQ_buf_b],bx	; update read pointer
irq_nempty:			; we did receive some data
	mov ah,0
	popf
	clc
irq_r_ret:
	pop bx
	ret

irq_empty1:
	push dx
	mov dx,[cs:comdta]	; if no data is found in the IRQ buffer,
	add dx,5		; check for incoming data by polling
	in al,dx
	test al,1		; bit is "data available" (drdy)
	jz irq_empty2
	sub dx,5
	in al,dx		; read received data
	pop dx
	jmp short irq_nempty

irq_empty2:			; there was REALLY no data available
	pop dx
	popf
	stc
        mov ax,0ffffh
	jmp short irq_r_ret


IRQ_WRITEBUF:			; write data to input buffer (from AL)
	push bx			; return CARRY if was not possible (jammed)
        mov bx,[cs:IRQ_buf_a]	; the write pointer
        mov [bx],al
        call IRQ_nextpos        ; BX circular + 1
        cmp bx,[cs:IRQ_buf_b] 	; catching up with read pointer? bad!
        jz irq_full     	; we could not put this char into the buffer
        mov [IRQ_buf_a],bx	; update write pointer
        clc
        jmp short irq_w_ret
irq_full:
        stc
irq_w_ret:
	pop bx
        RET

; ---------------------------------------------------------------------

IRQ_READIRQ:		; the main IRQ handler, caring for wishes of the UART 
        push ax
        push bx
        push dx
        push ds
	push BP
        mov ax,cs
        mov ds,ax
	xor BP,BP
irq_poll:
	inc BP
	test BP,07f00h
	jz n_irq_pech	; we do not believe in more than 256 actions per IRQ
	jmp irq_pech
n_irq_pech:
        mov dx,[cs:comdta]	; port used
        inc dx
	inc dx
        in al,dx        ; read IRQ status - tells UART that we care for it...
        dec dx
	dec dx
        test al,1
	jz irq_pending	; 0 == IRQ pending
	jmp irq_pech	; 1 == no more IRQ pending
irq_pending:
        and ax,6	; check for reason of IRQ:
		push es
		push bx
		mov bx,[cs:screenseg]
		mov es,bx
		mov bx,4000-8	; <<< lower right corner...
		mov byte [es:bx-2],'*'
		add bx,ax
		inc byte [es:bx]	; <<< *** messy IRQ indicators...
		pop bx
		pop es
	cmp al,4	; 0,2,4,6=modem status changed, data transmittable,
        jz irq_lesen	; new data arrived, line status changed
	cmp al,2
	jz irq_poll	; <<< *** we could use the opportunity to send data
			; <<< but then we would need a send buffer for that!
			; instead we go to "check for further IRQ reasons"
	cmp al,0
	jnz irq_line	; else it is 6: irq line status changed

irq_modem:		; 0: modem status changed
        add dx,6
        mov [comstats],al	; store new modem status
	jmp short irq_poll	; check for further IRQ reasons

irq_line:
	add dx,5
	in al,dx
	mov [comstats+1],al	; store new line status
	jmp short irq_poll	; check for further IRQ reasons

irq_lesen:
	or BP,8000h	; flag: we are supposed to read something
irq_les2:
        mov bx,[cs:IRQ_buf_a]        ; check buffer write pointer
        call IRQ_nextpos
        cmp bx,[cs:IRQ_buf_b]        ; is there still space in the buffer?
        jnz irq_les4

irq_les3:
	test BP,8000h	; have not read anything but buffer is full?
	jz i_poll	; nope, have read something
	in al,dx	; READ at least something anyway, will LOOSE data!
	mov bx,[cs:IRQ_buf_a]	; the write pointer (will NOT be updated)
	mov [cs:bx],al	; read char will OVERWRITE the last received char
		push es
		push bx
		mov bx,[cs:screenseg]
		mov es,bx
		mov bx,4000-8	; <<< lower right corner...
		mov byte [es:bx-2],al	; <<< *** messy "spill notification"
		pop bx
		pop es
	jmp short i_poll	; stop reading data, check further reasons

irq_les4:
        add dx,5
        in al,dx        ; read status: is there data to be read?
	mov [comstats+1],al	; store new line status
        sub dx,5
        test al,1
	JZ i_poll	; *** no more data to read, check other IRQ reasons
	in al,dx	; fetch received data from UART
		call IRQ_WRITEBUF     ; and store it in our buffer
	and BP,7fffh	; we have at least read one byte, good
	inc BP		; indicate we did something
	test BP,07f80h	; we do not believe in more than 128 bytes to be read
        jz irq_les2     ; try to read more data from the queue
i_poll:	jmp irq_poll	; check for further IRQ reasons

irq_pech:
	pop BP
        pop ds
        pop dx
        pop bx
        mov al,20h
        out 20h,al	; tell 8259 that the IRQ has been handled
        pop ax
        IRET
	
