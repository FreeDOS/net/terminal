; handles special menu keys, so the user can modify the terminal setup

F_TASTEN:
        mov al,ah
        xor ah,ah
        cmp al,3bh      ; F1
        jb f_non	; no function key
        sub al,3bh
        cmp al,10
        jae f_non	; no function key either
        or al,al
        jz f_1
	dec al
	jz f_2
	dec al
	jz f_3
	dec al
	jz f_4
	dec al
	jz f_5
	dec al
	jz f_6
	dec al
	jz f_7
	dec al
	jz f_8
	dec al
	jz f_9
	jmp f_10
f_non:	jmp f_none	; not a valid key

f_1:    	call SHOW_ROTATE	; F1 circulates the status disp lines
        jmp f_none		; Disp (returns nothing)

f_2:    inc word [linespeed]	; F2 increases the line speed one step
        and word [linespeed],7  ; Sp+
                call SPEEDSET
        jmp f_stat

f_3:    xor word [hshake],20h	; F3 toggles sending of BREAK
                call TX_BRK
        jmp f_stat              ; BRK

f_4:    mov al,3                ; ETX:  F4 sends an ETX (^C) character
        jmp short f_ret		; RETURNS a char

f_5:    mov ax,[cs:crpolicy]	; tCR: F5 changes the way ENTER is sent
	inc ax			; (4 ways, F5 circulates through them)
	test ax,3
	jnz f_5x	; store new value
	sub ax,4	; we overflowed our 2 bits, fixing...
f_5x:   mov [crpolicy],ax	; store new value
        jmp short f_none

f_6:    xor word [crpolicy],8000h	; F6 changes the way CR and LF are
        jmp short f_stat        ; rCR	displayed: as is or in a way that
					; either of them acts like CR+LF

f_7:    xor word [hshake],8	; CTS: F7 toggles waiting for CTS handshake
	test word [hshake],8	; + DSR
	jnz f_stat
	xor word [hshake],4	; F7 now toggles waiting for DSR handshake
        jmp short f_stat	; after toggling the CTS handshake to off
				; (so we do a 4-cycle here...)

; f_8:    xor word [hshake],4	; DSR: F8 toggles waiting for DSR handshake
;         jmp short f_stat

f_8:	mov word [flag_exit],1	; EXIT: F8 to exit program
	jmp short f_stat

f_9:    xor word [hshake],2	; RTS: F9 toggles sending RTS handshake
        jmp short f_stat	; (see lines with "RTSSEND" in terminal.asm)

f_10:   mov ax,[cs:hshake]	; DTR: F10 toggles sending DTR handshake
        xor ax,1
        mov [hshake],ax
        mov word [modlines],0fe08h	; mask DTR to 0, and set it...
				; (8 means always set OUT2, int enable)
        and ax,1
        or [modlines],ax		; ...to value from AX then
                call MOD_LINES	; changes are sent to the modem right away

f_stat:         call SHOW_PROTO	; protocol setup has changed, update display
f_none: xor ax,ax		; return no char
f_ret:  RET
